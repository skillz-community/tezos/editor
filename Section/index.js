import React from 'react';
import cl from 'classnames';
import s from './index.module.css';

function Section({
  bottom, title, subtitle, children, right, className,
}) {
  return (
    <div className={s.root}>
      {title && (
        <div className={cl(s.titles, className)}>
          <div className={s.title}>
            <h2 style={{ margin: '0px' }}>{title}</h2>
            <div className={s.right}>{right}</div>
          </div>
          <span className={s.subtitle}>{subtitle}</span>
        </div>
      )}
      {children}
      {bottom && <hr className={s.divider} />}
    </div>
  );
}

export default Section;
