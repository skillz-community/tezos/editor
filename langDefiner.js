const hljsDefineSmartpy = require('highlight.js/lib/languages/python');

function hljsDefineMichelson(hljs) {
  return {
    keywords: {
      keyword: 'storage parameter code key unit signature option list se operation contract pair or lambda map big_map chain_id int nat string bytes mutez bool key_hash timestamp address pair',
      built_in: 'Unit True False Pair Left Right Some None',
      literal: 'DROP DUP SWAP DIG PUSH SOME NONE UNIT IF_NONE PAIR CAR CDR SETCAR SETCDR LEFT RIGHT IF_LEFT NIL CONS IF_CONS SIZE EMPTY_SET EMPTY_MAP EMPTY_BIG_MAP MAP ITER MEM GET UPDATE IF LOOP LOOP_LEFT LAMBDA EXEC DIP FAILWITH CAST RENAME CONCAT SLICE PACK UNPACK ADD SUB MUL EDIV ABS ISNAT INT NEG LSL LSR OR AND XOR NOT COMPARE EQ NEQ LT GT LE GE SELF CONTRACT TRANSFER_TOKENS SET_DELEGATE CREATE_ACCOUNT CREATE_CONTRACT IMPLICIT_ACCOUNT NOW AMOUNT BALANCE CHECK_SIGNATURE BLAKE2B SHA256 SHA512 HASH_KEY STEPS_TO_QUOTA SOURCE SENDER ADDRESS CHAIN_ID ASSERT ASSERT_EQ ASSERT_NEQ ASSERT_GT ASSERT_LT ASSERT_GE ASSERT_LE ASSERT_NONE ASSERT_SOME ASSERT_LEFT ASSERT_RIGHT ASSERT_CMPEQ ASSERT_CMPNEQ ASSERT_CMPGT ASSERT_CMPLT ASSERT_CMPGE, ASSERT_CMPLE IFCMPEQ IFCMPNEQ IFCMPLT IFCMPGT IFCMPLE IFCMPGE CMPEQ CMPNEQ CMPLT CMPGT CMPLE CMPGE IFEQ IFNEQ IFLT IFGT IFLE IFGE UNPAIR UNPAPAIR IF_SOME FAIL DIIP DUUP',
    },
    contains: [
      {
        className: 'string',
        begin: '"',
        end: '"',
      },
      hljs.COMMENT(
        '#',
        '$',
      ),
      {
        className: 'number',
        begin: '-?[0-9]+',
      },
      {
        className: 'number',
        begin: '0x[0-9a-fA-F]+',
      },
      {
        className: 'keyword',
        begin: '[:%@][a-zA-Z_]+',
      },
    ],
  };
}

module.exports = (hljs) => {
  hljs.registerLanguage('michelson', hljsDefineMichelson);
  hljs.registerLanguage('smartpy', hljsDefineSmartpy);
};
