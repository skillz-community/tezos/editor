import React, { useState, useCallback } from 'react';
import Editor from 'react-simple-code-editor';
import {
  Select, MenuItem, FormControl, TextField, Button,
} from '@material-ui/core';
import hljs from 'highlight.js/lib/core';
import languages from './langDefiner';
import s from './index.module.css';
import Section from '../../../../../components/UI/Section';
import Available from './contracts.json';

languages(hljs);

const availableLanguages = ['smartpy', 'michelson'];

function format(txt) {
  return txt.replace(/\\n/g, '\n').replace(/\\"/g, '"');
}

function SmartpyTranspiler({ endpoint }) {
  const [template, setTemplate] = useState(0);
  const [language, setLanguage] = useState(0);
  const [content, setContent] = useState(format(Available[0].content));
  const [className, setClassName] = useState('');
  const [michelsonCode, setMichelsonCode] = useState(<div />);

  const onSetTemplate = useCallback(newIdx => {
    setMichelsonCode(<div />);
    setTemplate(newIdx);
    setContent(format(Available[newIdx].content));
  }, []);

  const transpileMichelson = async (event) => {
    event.preventDefault();
    await fetch(`${endpoint}/trans/${availableLanguages[language]}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        data: content,
        className,
      }),
    })
      .then(async (response) => {
        const json = await response.json();
        if (response.ok === false) {
          setMichelsonCode(<div />);
          window.error('Error during compilation.');
        } else {
          setMichelsonCode(
            <Editor
              style={{ marginTop: '1em' }}
              padding={15}
              className={s.editor}
              value={json.michelson}
              onValueChange={value => value}
              highlight={code => hljs.highlight(availableLanguages[1], code).value}
            />,
          );
          window.success('Compiled.');
        }
      });
  };

  return (
    <div className={s.root}>
      <Section
        title="Edit a smart contract"
        subtitle={Available[template].description}
        right={(
          <FormControl className={s.select}>
            <Select
              value={template}
              onChange={ev => onSetTemplate(ev.target.value)}
            >
              {Available.map((contract, k) => (
                <MenuItem value={k} key={contract.name}>{contract.name}</MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
      />
      <div className={s.editor_container}>
        <Editor
          padding={15}
          className={s.editor}
          value={content}
          onValueChange={value => setContent(value)}
          highlight={code => hljs.highlight(availableLanguages[language], code).value}
        />
        <form onSubmit={transpileMichelson} style={{ paddingTop: '1em', boxSizing: 'content-box', display: 'inline-block' }}>
          <TextField required helperText="Compile to michelson" placeholder="ClassName" onChange={(event) => setClassName(event.target.value)} />
          <Button onClick={transpileMichelson} style={{ color: 'rgb(122, 59, 133)', marginLeft: '1em', textAlign: 'center' }}>Compile</Button>
        </form>
        {michelsonCode}
        <div className={s.languageoptions}>
          <FormControl fullWidth>
            <Select
              value={language}
              disableUnderline
              onChange={ev => setLanguage(ev.target.value)}
            >
              {availableLanguages.map((l, k) => (
                <MenuItem value={k} key={l}>{l}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
      </div>
    </div>
  );
}

export default SmartpyTranspiler;
